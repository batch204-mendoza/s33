fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))

// getting all to do list item
fetch('https://jsonplaceholder.typicode.com/todos/')
.then((response) => response.json())
.then((json) => {

	let list =json.map((todo => {
		return todo.title;
	}))
	console.log(list);
});

// getting a specific to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) =>response.json())
.then((json) => console.log(`the item "${json.title}" on the list has  a status of ${json.completed}`));
//creating a to do list item using post method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body:JSON.stringify({
		title: 'Created a to do list item',
		completed: false,
		userId:1

	})
})
.then((response) => response.json())
.then((json) => console.log(json))


//updating a to do list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers:{
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to do list item',
		description : 'to update the my to do list with a different data structure',
		status: 'pending',
		dateCompleted: 'Pending',
		userId: 1
	})
})
.then((response) => response.json())
.then((json)=> console.log(json));

// updating to do list item using patch method.

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		status: 'complete',
		dateCompleted: '30/09/2022'	
	})
})

.then((response) => response.json())
.then((json) => console.log(json));
//deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'DELETE'
});

